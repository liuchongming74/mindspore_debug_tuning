![MindSpore标志](https://gitee.com/mindspore/mindspore/raw/master/docs/MindSpore-logo.png "MindSpore logo")

# MindSpore Tuning Tutorial

This experiment is designed based on **HUAWEI** cloud service ***ModelArts*** for **MindSpore**.

User want to run the MindSpore on ***ModelArts***, first of all, user has to convert pre-trained BERT ONNX model with
**MindInsight**'s sub-module **MindConverter**, and put the converted ckpt file into `./data` folder. 

The BERT ONNX model can be
downloaded from [here](https://lau-hdc-demo.obs.cn-north-4.myhuaweicloud.com/experiment%20resources/bert_zh.onnx), and conversion command is as follows:

```shell
mindconverter --model_file bert_zh.onnx --shape 1,512 1,512 1,512  \
              --input_nodes input_ids attention_mask token_type_ids  \
              --output_nodes output_0 output_1
```

If there's no available environment to do conversion, user can download converted ckpt 
from [here](https://lau-hdc-demo.obs.cn-north-4.myhuaweicloud.com/experiment%20resources/converted_model.tar.gz),
decompression the downloaded file and put `bert_zh.ckpt` into `./data` folder.

Please put dataset and pre-trained model into `./data` folder. Make sure the following files are existed in `./data`:

```bash
bert_zh.ckpt
train.mindrecord
train.mindrecord.db
test.mindrecord
test.mindrecord.db
```

## How to download the project

Use the following command to download the repo:

```bash
git clone https://gitee.com/liuchongming74/mindspore_debug_tuning.git
```

Dataset has been placed in this repo, if user has problem to download whole project with `git clone` command, user could
try to download repo with zip file.

![screenshot.png](screenshot.png)

## Download site of experiment resources

### Pre-trained BERT ONNX model download site:

- [Download link 1](https://lau-hdc-demo.obs.cn-north-4.myhuaweicloud.com/experiment%20resources/bert_zh.onnx)

- [Download link 2](https://weirenzheng.obs.cn-north-1.myhuaweicloud.com/MindSpore%E8%B0%83%E8%AF%95%E8%B0%83%E4%BC%98%E6%94%BB%E7%95%A5-bert_zh.onnx.rar)

### Converted pre-trained model download site:

- [Download link 1](https://lau-hdc-demo.obs.cn-north-4.myhuaweicloud.com/experiment%20resources/converted_model.tar.gz)

- [Download link 2](https://weirenzheng.obs.cn-north-1.myhuaweicloud.com/MindSpore%E8%B0%83%E8%AF%95%E8%B0%83%E4%BC%98%E6%94%BB%E7%95%A5-converted_model.tar.gz.rar)

### Backup download link of dataset:

- [Download link](https://lau-hdc-demo.obs.cn-north-4.myhuaweicloud.com/experiment%20resources/dataset.zip)
