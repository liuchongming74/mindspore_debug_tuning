# MindSpore IDE Plugin Download tutorial

## Download Site

[MindSpore IDE Plugin Download link](https://lau-hdc-demo.obs.cn-north-4.myhuaweicloud.com/experiment%20resources/MindSpore_IDE_Plugin.zip)

> **Notice:**
> 1. **This IDE plugin is only for demonstration.**
> 2. Please make sure the version of PyCharm is after 2020.03, otherwise, the IDE plugin may not work properly.
> 3. The plugin provided here for demonstration is not been fully tested on macOS.

## Installation tutorial

1. Open PyCharm IDE, goto **"Settings/Preferences"** -> **"Plugins"** -> **"Install plugin from disk..."**.

2. Restart PyCharm IDE, and **"MindSpore"** item will appear in PyCharm menu bar.

If time permits, please scan the QR code or click the [link](https://www.wjx.cn/vm/h7DcDQ0.aspx) to fill out this 
 questionnaire to help us do better.

![Feedback](https://lau-hdc-demo.obs.cn-north-4.myhuaweicloud.com/experiment%20resources/ide%20plugin%20feedback.png)
